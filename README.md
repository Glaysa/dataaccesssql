﻿
# Assignment 2: Data Access and SQL Scripts

## Tasks
**Task 1**: Write sql scripts.

**Task 2**: 
- Use SQL Client and implement CRUD operations on a the _Chinook_ database.
- Must be implemented with Repostory pattern.

## Tools
- .NET 5
- Visual Studio 2022

## Constributors:
- Glaysa Fernandez
- [Odin Kvarving](https://gitlab.com/odinkvarving)

