INSERT INTO Power (Name, Description) VALUES ('Laser Eyes', 'Shoots a beam of lasers out of the heroes eyes');
INSERT INTO Power (Name, Description) VALUES ('Light-speed', 'Makes the hero run as fast as light');
INSERT INTO Power (Name, Description) VALUES ('Super Strength', 'Makes the hero incredibly strong');
INSERT INTO Power (Name, Description) VALUES ('Invisibility', 'Makes the hero invisible at will');

INSERT INTO SuperheroPower (Superhero_id, Power_id) VALUES (1, 1);
INSERT INTO SuperheroPower (Superhero_id, Power_id) VALUES (2, 1);
INSERT INTO SuperheroPower (Superhero_id, Power_id) VALUES (3, 2);
INSERT INTO SuperheroPower (Superhero_id, Power_id) VALUES (3, 3);



