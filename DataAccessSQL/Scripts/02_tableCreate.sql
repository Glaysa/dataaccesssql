CREATE TABLE Superhero (
    Id int NOT NULL IDENTITY,
    Name varchar(255) NOT NULL,
    Alias varchar(255) NOT NULL,
    Origin varchar(255) NOT NULL,
    CONSTRAINT superhero_pk PRIMARY KEY (id)
);



CREATE TABLE Assistant (
    Id int NOT NULL IDENTITY,
    Name varchar(255),
	Superhero_id int
    CONSTRAINT assistant_pk PRIMARY KEY (id)
);


CREATE TABLE Power (
    Id int NOT NULL IDENTITY,
    Name varchar(255),
    Description varchar(255),
    CONSTRAINT power_pk PRIMARY KEY (id)
);

CREATE TABLE SuperheroPower (
	Superhero_id int NOT NULL,
	Power_id int NOT NULL,
);