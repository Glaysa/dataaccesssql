ALTER TABLE SuperheroPower ADD FOREIGN KEY (Superhero_id) REFERENCES Superhero(Id);
ALTER TABLE SuperheroPower ADD FOREIGN KEY (Power_id) REFERENCES Power(Id);
ALTER TABLE SuperheroPower ADD CONSTRAINT PK_id PRIMARY KEY (Superhero_id,Power_id);