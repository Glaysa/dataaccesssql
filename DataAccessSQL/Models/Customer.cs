﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessSQL.Models
{
    internal class Customer
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Country { get; set; }

        public string? PostalCode;

        public string? Phone;

        public string? Email;
        public Customer()
        {

        }

        public Customer(string firstName, string lastName, string country, string postalCode, string phone, string email)
        {
            FirstName = firstName;
            LastName = lastName;
            Country = country;
            PostalCode = postalCode;
            Phone = phone;
            Email = email;
        }

        /// <summary>
        /// ToString() containing the different attributes for a Customer
        /// </summary>
        /// <returns>A string containing the attributes</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"First name: {FirstName}");
            sb.AppendLine($"Last name: {LastName}");
            sb.AppendLine($"Country name: {Country}");
            sb.AppendLine($"Postal code: {PostalCode}");
            sb.AppendLine($"Phone number: {Phone}");
            sb.AppendLine($"Email: {Email}");
            sb.AppendLine();
            return sb.ToString();

        }

    }
}
