﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessSQL.Models
{
    internal class CustomerGenre
    {

        public int CustomerId { get; set; }

        public string Genre { get; set; }

    }
}
