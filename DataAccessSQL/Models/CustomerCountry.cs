﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessSQL.Models
{
    internal class CustomerCountry
    {

        public int NumberOfCustomers { get; set; }

        public string CountryName { get; set; }

        public CustomerCountry()
        {
        }
    }
}
