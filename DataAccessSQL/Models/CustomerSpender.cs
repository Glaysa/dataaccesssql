﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessSQL.Models
{
    internal class CustomerSpender
    {
        public int CustomerId { get; set; }

        public double InvoiceTotal { get; set; }
    }
}
