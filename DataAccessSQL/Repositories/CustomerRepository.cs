﻿using DataAccessSQL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace DataAccessSQL.Repositories
{
    internal class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Method for inserting a new Customer to the database
        /// </summary>
        /// <param name="customer">The Customer to insert into the database</param>
        /// <returns>True or false</returns>
        public bool AddNewCustomer(Customer customer)
        {
            string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) VALUES (@firstName, @lastName, @country, @postalCode, @phone, @email)";
            try
            {
                using SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString());
                connection.Open();
                using SqlCommand command = new SqlCommand(sql, connection);

                command.Parameters.AddWithValue("firstName", customer.FirstName);
                command.Parameters.AddWithValue("lastName", customer.LastName);
                command.Parameters.AddWithValue("country", customer.Country);
                command.Parameters.AddWithValue("postalCode", customer.PostalCode);
                command.Parameters.AddWithValue("phone", customer.Phone);
                command.Parameters.AddWithValue("email", customer.Email);

                int affectedRows = command.ExecuteNonQuery();
                if (affectedRows < 1) throw new Exception($"Customer failed to save in the database.");
                if (affectedRows > 1) throw new Exception($"More than 1 row is inserted!");

                Console.WriteLine($"A new customer has been added to the database!");
                return true;
            }
            catch (SqlException sqex)
            {
                Console.WriteLine(sqex.Message);
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Method for fetching all Customers from the database
        /// </summary>
        /// <returns>A List of Customers containing all customers from the database</returns>
        public List<Customer> GetAllCustomers()
        {

            List<Customer> customers = new List<Customer>();
            string sql = "SELECT FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

            try
            {
                using SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString());
                connection.Open();
                using SqlCommand command = new SqlCommand(sql, connection);

                using SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    customers.Add(fetchData(reader));
                }
                foreach (Customer customer in customers) Console.WriteLine(customer.ToString());
            }
            catch (SqlException sqex)
            {
                Console.WriteLine(sqex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customers;
        }

        /// <summary>
        /// Method for fetching a specific Customer by ID
        /// </summary>
        /// <param name="id">The ID of the Customer</param>
        /// <returns>The Customer that was found with the given ID</returns>
        public Customer GetCustomerById(int id)
        {
            Customer customer = new Customer();
            string sql = "SELECT FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = @id";

            try
            {
                using SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString());
                connection.Open();

                using SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@id", id);

                using SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    customer = fetchData(reader);
                }

                if (!reader.HasRows) throw new Exception($"No customer found with the id {id}.");

                Console.WriteLine(customer.ToString());
            }
            catch (SqlException sqex)
            {
                Console.WriteLine(sqex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }

        /// <summary>
        /// Method for fetching a Customers with a specific name (first or last). Will also return where only parts of the given name matches
        /// </summary>
        /// <param name="firstName">The first name to check for</param>
        /// <param name="lastName">The last name to check for</param>
        /// <returns>A Customer matching the given name</returns>
        public Customer GetCustomerByName(string firstName, string lastName)
        {
            Customer customer = new Customer();
            string sql = "SELECT FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE @firstName OR LastName LIKE @lastName";

            try
            {
                using SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString());
                connection.Open();
                using SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("firstName", $"%{firstName}%");
                command.Parameters.AddWithValue("lastName", $"%{lastName}%");

                using SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    customer = fetchData(reader);
                }
                
                if (!reader.HasRows) throw new Exception($"No customer found with a firstname that matches '{firstName}' or a lastname that matches '{lastName}'.");

                Console.WriteLine(customer.ToString());
            }
            catch (SqlException sqex)
            {
                Console.WriteLine(sqex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }

        /// <summary>
        /// Method for fetching a List of Customers within a given interval (specified offset and limit)
        /// </summary>
        /// <param name="offset">Number of rows to skip before fetching</param>
        /// <param name="limit">Number of Customers to fetch from offset</param>
        /// <returns>A List of Customers within the given interval</returns>
        public List<Customer> GetCustomersByPage(int offset, int limit)
        {
            List<Customer> customers = new List<Customer>();
            string sql = "SELECT FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer " +
                "ORDER BY CustomerId " +
                "OFFSET @offset ROWS " +
                "FETCH NEXT @limit ROWS ONLY";

            try
            {
                using SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString());
                connection.Open();
                using SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("offset", offset);
                command.Parameters.AddWithValue("limit", limit);

                using SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    customers.Add(fetchData(reader));
                }
                foreach (Customer customer in customers) Console.WriteLine(customer.ToString());
            }
            catch (SqlException sqex)
            {
                Console.WriteLine(sqex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customers;
        }

        /// <summary>
        /// Method for fetching a List of Customers that are the highest spenders in the Invoice-table with a descending order
        /// </summary>
        /// <returns>A List of Customers containing their ID and Invoice-total</returns>
        public List<CustomerSpender> GetHighestSpenders()
        {
            List<CustomerSpender> customers = new List<CustomerSpender>();
            string sql = "SELECT CustomerId, SUM(Total) AS Total FROM Invoice GROUP BY CustomerId ORDER BY SUM(Total) DESC";

            try
            {
                using SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString());
                connection.Open();
                using SqlCommand command = new SqlCommand(sql, connection);
                
                using SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    CustomerSpender customerSpender = new CustomerSpender();
                    customerSpender.CustomerId = reader.GetInt32(reader.GetOrdinal("CustomerId"));
                    customerSpender.InvoiceTotal = (double)reader.GetDecimal(reader.GetOrdinal("Total"));
                    customers.Add(customerSpender);
                }
                foreach (CustomerSpender customer in customers) Console.WriteLine($"Customer ID: {customer.CustomerId} Invoice total: {customer.InvoiceTotal}");
            }
            catch (SqlException sqex)
            {
                Console.WriteLine(sqex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customers;
        }

        /// <summary>
        /// Method for fetching the most popular genre for a specific Customer given their ID
        /// </summary>
        /// <param name="customerID">The ID of the Customer</param>
        /// <returns>A List containing the ID of the Customer and their most popular genre(s)</returns>
        public List<CustomerGenre> GetMostPopularGenreForCustomer(int customerID)
        {
            List<CustomerGenre> customers = new List<CustomerGenre>();
            string sql = "SELECT TOP 1 WITH TIES Genre.Name AS Genre, CustomerId FROM Genre " +
                "INNER JOIN Track ON Genre.GenreId = Track.GenreId " +
                "INNER JOIN InvoiceLine ON InvoiceLine.TrackId = Track.TrackId " +
                "INNER JOIN Invoice ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                "WHERE Invoice.CustomerId = @customerId " +
                "GROUP BY Genre.Name, Invoice.CustomerId " +
                "ORDER BY COUNT(Genre.GenreId) " +
                "DESC";

            try
            {
                using SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString());
                connection.Open();

                using SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@customerId", customerID);

                using SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    CustomerGenre customerGenre = new CustomerGenre();
                    customerGenre.CustomerId = reader.GetInt32(reader.GetOrdinal("CustomerId"));
                    customerGenre.Genre = reader.GetString(reader.GetOrdinal("Genre"));
                    customers.Add(customerGenre);
                }

                if (!reader.HasRows) throw new Exception($"No customer found with the id {customerID}.");

                foreach (CustomerGenre customer in customers) Console.WriteLine($"Customer ID: {customer.CustomerId} Genre: {customer.Genre}");
            }
            catch (SqlException sqex)
            {
                Console.WriteLine(sqex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customers;
        }

        /// <summary>
        /// Method for fetching the number of Customers from each country in a descending order
        /// </summary>
        /// <returns>A List containing the name of the countries and their number of Customers</returns>
        public List<CustomerCountry> GetNumberOfCustomersFromCountry()
        {
            List<CustomerCountry> customers = new List<CustomerCountry>();
            string sql = "SELECT Country, COUNT(DISTINCT CustomerId) " +
                "AS NumberOfCustomers FROM Customer " +
                "GROUP BY Country " +
                "ORDER BY COUNT(CustomerId) DESC;";

            try
            {
                using SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString());
                connection.Open();
                using SqlCommand command = new SqlCommand(sql, connection);
                
                using SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    CustomerCountry customerCountry = new CustomerCountry();
                    customerCountry.CountryName = reader.GetString(reader.GetOrdinal("Country"));
                    customerCountry.NumberOfCustomers = reader.GetInt32(reader.GetOrdinal("NumberOfCustomers"));
                    customers.Add(customerCountry);
                }
                foreach (CustomerCountry customer in customers) Console.WriteLine($"Name of country: {customer.CountryName} \nNumber of customers: {customer.NumberOfCustomers}\n");
            }
            catch (SqlException sqex)
            {
                Console.WriteLine(sqex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customers;
        }

        /// <summary>
        /// Method for updating a specific Customer based on their ID
        /// </summary>
        /// <param name="customerID">The ID of the Customer to update</param>
        /// <param name="updatedCustomer">The updated Customer</param>
        /// <returns>True or false</returns>
        public bool UpdateCustomer(int customerID, Customer updatedCustomer)
        {
            string sql = "UPDATE Customer SET FirstName = @firstName, LastName = @lastName, Country = @country, PostalCode = @postalCode, Phone = @phone, Email = @email WHERE CustomerId = @customerId";
            try
            {

                using SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString());
                connection.Open();
                using SqlCommand command = new SqlCommand(sql, connection);

                command.Parameters.AddWithValue("firstName", updatedCustomer.FirstName);
                command.Parameters.AddWithValue("lastName", updatedCustomer.LastName);
                command.Parameters.AddWithValue("country", updatedCustomer.Country);
                command.Parameters.AddWithValue("postalCode", updatedCustomer.PostalCode);
                command.Parameters.AddWithValue("phone", updatedCustomer.Phone);
                command.Parameters.AddWithValue("email", updatedCustomer.Email);
                command.Parameters.AddWithValue("customerId", customerID);

                int affectedRows = command.ExecuteNonQuery();
                if (affectedRows < 1) throw new Exception($"No customer found with the id {customerID}.");
                if (affectedRows > 1) throw new Exception($"More than 1 row is updated!");

                Console.WriteLine($"Updated customer with id: {customerID}.");
                return true;
            }
            catch (SqlException sqex)
            {
                Console.WriteLine(sqex.Message);
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Method for reading the data from a SqlDataReader and assigning it to a Customer-object 
        /// </summary>
        /// <param name="reader">The SqlDataReader</param>
        /// <returns>The Customer-object where the data was assigned</returns>
        public Customer fetchData(SqlDataReader reader)
        {
            Customer temp = new Customer();
            try
            {
                temp.FirstName = reader["Firstname"].ToString();
                temp.LastName = reader["LastName"].ToString();
                temp.Country = reader["Country"].ToString();
                temp.PostalCode = reader["PostalCode"].ToString();
                temp.Phone = reader["Phone"].ToString();
                temp.Email = reader["Email"].ToString();
            }
            catch (SqlException sqex)
            {
                Console.WriteLine(sqex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return temp;
        }
    }
}
