﻿using DataAccessSQL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessSQL.Repositories
{
    internal interface ICustomerRepository
    {
        public List<Customer> GetAllCustomers();

        public Customer GetCustomerById(int id);

        public Customer GetCustomerByName(string firstName, string lastName);

        public List<Customer> GetCustomersByPage(int offset, int limit);

        public bool AddNewCustomer(Customer customer);

        public bool UpdateCustomer(int customerID, Customer updatedCustomer);

        public List<CustomerCountry> GetNumberOfCustomersFromCountry();

        public List<CustomerSpender> GetHighestSpenders();

        public List<CustomerGenre> GetMostPopularGenreForCustomer(int customerID);
    }
}
